#!/bin/env sh

# Executables
export PATH=~/.cargo/bin:$PATH
export PATH=~/.go/bin:$PATH
export PATH=~/setup/bin:$PATH

# Flatpak
export PATH=~/var/lib/flatpak/exports/bin:$PATH
alias flatpak="flatpak --user"